﻿using Serilog;

namespace CustomerApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // Konfigurasi Serilog
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();

            // Tambahkan logging ke pipeline
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddSerilog();
            });

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            // Tambahkan middleware logging Serilog
            app.UseSerilogRequestLogging();

        }


    }
}
