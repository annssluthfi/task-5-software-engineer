﻿using CustomerApi.EfCore;
using CustomerApi.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CustomerApi.Controllers
{
    [ApiController]
    public class customersController : ControllerBase
    {
        private readonly EF_DataContext _context;
        private static readonly HttpClient client = new HttpClient();

        private readonly ILogger<customersController> _logger;



        public customersController(EF_DataContext context, ILogger<customersController> logger)
        {
            _context = context;
            _logger = logger;
        }

        //GetAllData
        [Route("api/[controller]")]
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var customers = _context.Customers.ToList();

                if (customers == null || customers.Count == 0)
                    return new JsonResult(NotFound());

                return new JsonResult(Ok(customers));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while retrieving customer's data.");
                return StatusCode(StatusCodes.Status500InternalServerError,
                                  "An error occurred while retrieving customers's data.");
            }


        }

        //GetDataById
        [Route("api/[controller]/{Id}")]
        [HttpGet]
        public IActionResult GetDatabyId(int Id)
        {
            try
            {
                //cek apakah id ada di dalam database atau tidak
                var result = _context.Customers.Find(Id);
                //jika tidak ada maka akan return not found status code
                if (result == null)
                    return new JsonResult(NotFound());
                //jika ada maka akan return status code ok dan data dalam bentuk json
                return new JsonResult(Ok(result));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while retrieving customer's data by Id.");
                return StatusCode(StatusCodes.Status500InternalServerError, $"An error occurred while retrieving data by ID: {ex.Message}");
            }
        }

        //Create Data
        [Route("api/[controller]")]
        [HttpPost]
        public IActionResult Post(Customer customer)
        {
            try
            {
                if (customer.Id == 0)
                {
                    _context.Customers.Add(customer);
                    _context.SaveChanges();

                    return new JsonResult(Ok());
                }
                else
                {
                    return new JsonResult(NotFound());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while creating customer's data.");
                return StatusCode(StatusCodes.Status500InternalServerError,
                                                  "An error occurred while creating customers's data.");
            }
        }

        //Put Data (memperbarui data customer)
        [Route("api/[controller]/{Id}")]
        [HttpPut]
        public IActionResult Put(CustomerModel customer)
        {
            try
            {
                if (customer.Id != 0)
                {
                    var customerInDb = _context.Customers.Find(customer.Id);

                    if (customerInDb != null)
                    {
                        //jika ditemukan data, lakukan pembaruan
                        customerInDb.FirstName = customer.FirstName;
                        customerInDb.LastName = customer.LastName;
                        customerInDb.Email = customer.Email;
                        customerInDb.HomeAddress = customer.HomeAddress;

                        _context.SaveChanges();

                        return new JsonResult(customer);
                    }
                }

                return new JsonResult(NotFound());
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "An error occurred while editing customer's data.");
                return StatusCode(StatusCodes.Status500InternalServerError,
                                                  "An error occurred while editing customers's data.");
            }
        }

        [Route("api/[controller]/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            try
            {
                var result = _context.Customers.Find(Id);

                if(result == null)
                    return new JsonResult(NotFound());

                _context.Customers.Remove(result);
                _context.SaveChanges();

                var customers = _context.Customers.ToList();
                return new JsonResult(Ok(customers));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "An error occurred while deleting customer's data.");
                return StatusCode(StatusCodes.Status500InternalServerError,
                                                  "An error occurred while deleting customers's data.");
            }
        }

        [Route("api/todos")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {

            try
            {
                HttpResponseMessage response = await client.GetAsync("https://jsonplaceholder.typicode.com/posts");

                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();

                return Ok(responseBody);
            }
            catch(HttpRequestException ex)
            {
                _logger.LogError(ex, "An error occurred.");
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

        [Route("api/todos/{id}")]
        [HttpGet]
        public IActionResult GetById(int id)
        {
            try
            {
                // Mengirim permintaan GET ke API eksternal secara synchronous dengan ID yang diminta
                HttpResponseMessage response = client.GetAsync($"https://jsonplaceholder.typicode.com/posts/{id}").Result;

                // Memeriksa kode status respons
                response.EnsureSuccessStatusCode();

                // Membaca konten respons sebagai string secara synchronous
                string responseBody = response.Content.ReadAsStringAsync().Result;

                // Lakukan pengolahan data sesuai kebutuhan Anda
                return new JsonResult(responseBody);
            }
            catch (HttpRequestException ex)
            {
                _logger.LogError(ex, "An error occurred.");
                return StatusCode(500, $"Error: {ex.Message}");
            }
        }

    }
}
