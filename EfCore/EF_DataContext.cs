﻿using Microsoft.EntityFrameworkCore;
using CustomerApi.Models;

namespace CustomerApi.EfCore
{
    public class EF_DataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public EF_DataContext(DbContextOptions<EF_DataContext> options): base(options) { }

    }
}
